define([
	'fs'
], function(
	fs
) {
	return {
		name: 'Wiki Generator',

		extraScripts: [
			'spells'
		],

		spellsWaiting: true,

		init: function () {
			if(process.argv[1].endsWith('worker')) {
    			return;
			}

			this.spells = require(`${this.relativeFolderName}/spells`);

			this.events.on('onBeforeGetClasses', this.classes.bind(this));
			this.events.on('onBeforeGetSkins', this.skins.bind(this));
			this.events.on('onBeforeGetItemTypes', this.itemTypes.bind(this));

			this.events.on('onBeforeGetSpellsConfig', this.spellsConfig.bind(this));
			this.events.on('onBeforeGetSpellsInfo', this.spellsInfo.bind(this));

			//this.events.on('onBeforeGetSpellTemplate', console.log.bind(this));
			//this.events.on('onBeforeGetResourceList',  console.log.bind(this));
			//this.events.on('onBeforeGetAnimations',    console.log.bind(this));
			//this.events.on('onAfterGetZone',           console.log.bind(this));
		},

		spellsConfig: function(spellsConfig) {
			this.sconf = spellsConfig;
			if(this.sconf && this.sinf && this.spellsWaiting) {
				this.spells();
			}
		},
		spellsInfo: function(spellsInfo) {
			this.sinf = spellsInfo;
			if(this.sinf && this.sconf && this.spellsWaiting) {
				this.spells();
			}
		},

		itemTypes: function(types) {
			var data = [];
			Object.keys(types).forEach(type => {
				data.push({
					type: type,
					data: types[type]
				})
			});

			var fileOut = [];

			data.forEach(slot => {
				var type = slot.type.split('')[0].toUpperCase() + slot.type.substr(1);
				var head = `== List of ${type} Items ==\n{| class="wikitable sortable" style="text-align:center"\n|-\n! Name !! Image !! Material\n|-`;
				var foot = `|}`;
				var out = [];

				Object.keys(slot.data).forEach(ex => {
					var mat = slot.data[ex].material || 'N/A';
					var filename = ex.replace(' ', '_') + '.png';

					out.push(`| ${ex} || [[File:${filename}|100px]] || ${mat}\n|-`);
				});

				out = head + `\n` + out.join(`\n`) + `\n` + foot;
				fileOut.push(out);
			});

			fs.writeFileSync(`${this.relativeFolderName}/output/itemTypes.txt`, fileOut.join(`\n`.repeat(10)), 'utf8');
		},

		classes: function(classes) {
			var head = `{| class="wikitable sortable" style="text-align:center"\n|-\n! Name !! Default Stat !! Starting HP !! Starting Spells !! Starting Weapon\n|-`;
			var foot = `|}`;
			var out = [];
			classes.list.forEach((c) => {
				var weapon = classes.weapons[c];

				var spells = classes.spells[c];
				spells.join(' | ').split(' ').forEach((spell, idx) => {
					spells[idx] = spell.split('')[0].toUpperCase() + spell.substr(1);
				});
				spells = spells.join(' ').split('|');
				spells = spells.map(s => `[[${s.trim()}]]`);

				var stats = classes.stats[c];
				var map = {
					str: 'strength',
					int: 'intellect',
					dex: 'dexterity'
				}
				var defaultStat = Object.keys(stats.gainStats)[0];
				defaultStat = map[defaultStat];
				defaultStat = defaultStat.split('')[0].toUpperCase() + defaultStat.substr(1);

				out.push(`| [[${c.split('')[0].toUpperCase() + c.substr(1)}]] || [[${defaultStat}]] || ${stats.values.hpMax} || ${spells.join(', ')} || [[${weapon}]]\n|-`);
			});

			out = head + `\n` + out.join(`\n`) + `\n` + foot;

			fs.writeFileSync(`${this.relativeFolderName}/output/classes.txt`, out, 'utf8');
		},

		skins: function(skins) {
			skins = Object.values(skins);

			var out = [];

			var head = `{| class="wikitable sortable" style="text-align:center"\n|-\n! Name !! Default? !! Image !! Notes\n|-`;
			var foot = `|}`;

			skins.forEach(skin => {
				var def = skin.default ? `Yes` : `No`;
				var filename = skin.name.toLowerCase().replace(' ', '') + '.png';

				var extras = [];

				if(skin.defaultSpirit) {
					extras.push("Default spirit for " + skin.defaultSpirit);
				}

				out.push(`| ${skin.name} || ${def} ||style="background-color:#2D2136" | [[File:${filename}|100px]] || ${extras.join('<br />')}\n|-`);
			});

			out = head + `\n` + out.join(`\n`) + `\n` + foot;

			fs.writeFileSync(`${this.relativeFolderName}/output/skins.txt`, out, 'utf8');
		}
	}
});
