define([
	'fs'
], function(
	fs
) {
	return function() {
		setTimeout(() => {
			var fileOut = [];

			infos = this.sinf;
			configs = this.sconf;

			infos.forEach(info => {
				var config = Object.keys(configs).find((c) => {
					var iname = info.name.toLowerCase().split(' ').join('');
					var cname = c.toLowerCase().split(' ').join('');
					return cname == iname;
				});
				config = configs[config];

				var rows = [];

				// title
				rows.push(`= ${info.name} =`);

				// table header
				rows.push(`{| class="wikitable" style="float:right; margin-left: 10px; text-align:center"`);
				rows.push(`! ${info.name}`);

				// images
				rows.push(`|-`);
				var filename = info.name.split(' ').join('_');
				rows.push(`| style="background-color:#2D2136" | [[File:${filename}.png|100px]]`);

				// description
				rows.push(`|-`);
				rows.push(`| ${info.description || '(No Description Found)'}`);

				// stat types
				if(config) {
					var map = {
						str: '[[Strength]]',
						int: '[[Intellect]]',
						dex: '[[Dexterity]]',
						'na': 'N/A'
					};
					if(typeof config.statType == 'string') {
						config.statType = [config.statType];
					}
					var stats = (config.statType || ['na']).map(s => map[s]);
					rows.push(`|-`);
					rows.push(`| Stat: ${stats.join(', ')}`);
				}

				// element
				if(config) {
					if(config.element) {
						rows.push(`|-`);
						rows.push(`| Element: ` + config.element);
					}
				}

				// cooldown, mana
				if(config) {
					if(config.manaCost) {
						rows.push(`|-`);
						rows.push(`| Mana Cost: ${config.manaCost}`);
					}
					if(config.cdMax) {
						rows.push(`|-`);
						rows.push(`| Cooldown: ${config.cdMax}`);
					}
				}

				// effects
				if(config) {
					if(config.effect) {
						var effects = {
							swiftness: 'Swiftness',
							regenHp: 'Regenerate HP',
							regenMana: 'Regenerate Mana'
						};

						rows.push(`|-`);
						rows.push(`| Effect: ${effects[config.effect]}`);
					}
					if(config.manaReserve) {
						if(config.manaReserve.percentage) {
							rows.push(`|-`);
							rows.push(`| Reserves ${config.manaReserve.percentage * 100}% mana`);
						}
					}
					if(config.needLos) {
						rows.push(`|-`);
						rows.push(`| Needs line-of-sight`);
					}
					if(config.range) {
						rows.push(`|-`);
						rows.push(`| Range: ${config.range}`);
					}
					if(config.auraRange) {
						rows.push(`|-`);
						rows.push(`| Aura Range: ${config.auraRange}`);
					}

					if(config.random) {
						if(config.random.damage) {
							rows.push(`|-`);
							rows.push(`| Damage: ${config.random.damage[0]} to ${config.random.damage[1]}`);
						}
						if(config.random.healing) {
							rows.push(`|-`);
							rows.push(`| Healing: ${config.random.healing[0]} to ${config.random.healing[1]}`);
						}
					}
				}

				// table footer
				rows.push(`|}`);

				// categorize
				rows.push(`[[Category:Item]][[Category:Rune]]`);

				fileOut.push(rows.join(`\n`));
			});

			fs.writeFileSync(`${this.relativeFolderName}/output/spells.txt`, fileOut.join(`\n`.repeat(10)), 'utf8');
		}, 1000);
	}
});
